﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Question2
    {
        static HttpClient client = new HttpClient();
        public static List<string> getUserNames(int submission)
        {
            return getAsyncUserNames(10).GetAwaiter().GetResult();
        }

        private static async Task<List<string>> getAsyncUserNames(int submission){

            List<Datum> apiList= new List<Datum>();

            HttpResponseMessage response = await client.GetAsync("https://jsonmock.hackerrank.com/api/article_users/search");
            if (response.IsSuccessStatusCode)
            {
                string json = await response.Content.ReadAsStringAsync();
                apiList = JsonConvert.DeserializeObject<Rootobject>(json).data.ToList();
            }
            return apiList.Where(x => x.submission_count > 10).Select(u => u.username).ToList();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Question3
    {
        // Question 3
        public class Person
        {
            public int Id { get; set; } = Guid.NewGuid().GetHashCode();
            public string Name { get; set; }
            public int? Age { get; set; }

            public void CalcAge(DateTime _birthDate)
            {
                this.Age = DateTime.Now.Year - _birthDate.Year;
            }
        }

        public class PersonManager
        {
            public static List<Person> DB { get; set; } = new List<Person>();

            public void AddPerson(string name, string birthDateString)
            {
                DateTime birthDate = DateTime.Parse(birthDateString);
                try
                {
                    Person p = new Person();
                    p.Name = name;
                    p.CalcAge(birthDate);
                    DB.Add(p);
                }
                catch (Exception e)
                {
                    throw;
                }
            }

            public void DeletePerson(int id)
            {
                try
                {
                    Person foundPerson = DB.FirstOrDefault(p => p.Id == id);
                    DB.Remove(foundPerson);
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }
    }
}

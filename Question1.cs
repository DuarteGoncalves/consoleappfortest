﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public static class Question1
    {
        public static string[] sortRoman(int arrayCount, string[] romanNames)
        {
            if(arrayCount < 1 || arrayCount > 50)
            {
                throw new ArgumentOutOfRangeException();
            }
            List<RomanName> romanNameList = new List<RomanName>();
            Regex romReg = new Regex(@"([A-Z])\w{1,20} (X|I|L|V){1,6}");
            foreach (var name in romanNames)
            {
                if (!romReg.IsMatch(name))
                {
                    throw new ArgumentException();
                }

                romanNameList.Add(new RomanName(name));
            }
            romanNameList = romanNameList.OrderBy(n => n.Number).ToList();
            romanNameList = romanNameList.OrderBy(n => n.Name).ToList();

            string[] sortedRomanNames = romanNameList.Select(x => x.FullName).ToArray();
            return sortedRomanNames;
        }
    }

    public class RomanName
    {
        private static Dictionary<char,int> CharValues =
            new Dictionary<char,int> {
                {'I',1},
                {'V',5},
                {'X',10},
                {'L',50}
            };
        public string FullName { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }

        public RomanName(string romanName)
        {
            FullName = romanName;

            string[] nameSplit = romanName.Split(' ');

            Name = nameSplit[0];

            Number = getNumber(nameSplit[1]);

        }

        private int getNumber(string roman)
        {
            int total = 0;
            int last_value = 0;
            for (int i = roman.Length - 1; i >= 0; i--)
            {
                int new_value = CharValues[roman[i]];
                if (new_value < last_value)
                    total -= new_value;
                else
                {
                    total += new_value;
                    last_value = new_value;
                }
            }
            return total;
        }
    }
}
